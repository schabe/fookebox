"""
This file provides a bunch of general utility classes for tests
"""
from __future__ import annotations

from asyncio import Queue, Task, all_tasks
from collections.abc import AsyncGenerator, Awaitable

from mpd.base import CommandError

from fookebox.config import load_config
from fookebox.mpd import MPDContext


List = list


async def all_tasks_done_except(tasks: set[Task]) -> None:
    """Wait for all current tasks to finish, except those specified as
    parameters"""
    current_tasks = all_tasks().difference(tasks)
    for task in current_tasks:
        coro = task.get_coro()
        assert isinstance(coro, Awaitable)
        await coro


class ConfigWrapper:
    """
    The ConfigWrapper wraps a fookebox.config object.

    In addition to the wrapped object, we provide a way to overwrite ('set')
    config values.
    """
    def __init__(self) -> None:
        # pylint: disable=missing-function-docstring
        self.cfg: dict[str, str | int] = {}
        self.cfg.update(load_config('tests/fixtures/config-empty.ini'))

    def set(self, key: str, value: str | int) -> None:
        # pylint: disable=missing-function-docstring
        if key not in self.cfg:
            # pylint: disable=broad-exception-raised
            raise Exception(f'Invalid config key: {key}')
        self.cfg[key] = value

    def get(self, key: str) -> str | int | None:
        # pylint: disable=missing-function-docstring
        return self.cfg.get(key)

    def getboolean(self, key: str) -> bool:
        # pylint: disable=missing-function-docstring
        return bool(self.cfg.get(key))

    def getint(self, key: str) -> int:
        # pylint: disable=missing-function-docstring
        return int(self.cfg.get(key, "0"))


# pylint: disable=too-many-instance-attributes
class MockMPD(MPDContext):
    """The MockMPD class crudely mocks an MPDClient."""

    def __init__(self) -> None:
        # pylint: disable=missing-function-docstring
        super().__init__('localhost', 0)
        self.mpd_host: str | None = None
        self.mpd_port: int | None = None
        self.mpd_consume = False
        self.mpd_password: str | None = None
        self.commands: list[str] = []
        self.mpd_queue: list[str] = []
        self.mpd_current_song: str | None = None
        self.mpd_playlists: dict[str, list] = {}
        self.mpd_status: dict[str, str] = {}
        self.mpd_files: list[dict] = []
        self.mpd_covers: dict[str, str] = {}
        self.idle_queue: Queue[list[str]] = Queue()

    async def __aenter__(self) -> MockMPD:
        return self

    async def __aexit__(self, *args: List | None) -> None:
        pass

    async def status(self) -> dict[str, str]:
        """Get the current status"""
        return self.mpd_status

    async def list(self, type_: str) -> List[dict[str, str]]:
        """List all items of specified type (eg. 'genre' or 'artist')"""
        items: list[str] = [x[type_] for x in self.mpd_files]
        unique: list[str] = list(set(items))
        return [{type_: x} for x in unique]

    async def find(self, *args: List[str] | None) -> List[dict[str, str]]:
        """Search MPD database. args is an arbitrary key/value combination"""
        hits = self.mpd_files

        for i in range(len(args) // 2):
            where = args[2 * i]
            what = args[2 * i + 1]
            hits = [row for row in hits if row.get(where) == what]

        return list(hits)

    async def listall(self) -> List[dict[str, str]]:
        """List all files on the server"""
        return self.mpd_files

    async def add(self, path: str) -> None:
        """Add PATH to playlist"""
        self.mpd_queue.append(path)

    async def playlistinfo(self) -> List[dict[str, str]]:
        """Get the current playlist (including current song)"""
        queue: List[dict[str, str]] = [{'file': x} for x in self.mpd_queue]
        if self.mpd_current_song:
            queue.insert(0, {'file': self.mpd_current_song})

        return queue

    async def listplaylist(self, name: str) -> List[str]:
        """Get a playlist by its name"""
        return self.mpd_playlists.get(name, [])

    async def playlist(self) -> List[str]:
        """Get the current playlist (including current song)"""
        playlist = self.mpd_queue[:]

        if self.mpd_current_song:
            playlist.insert(0, self.mpd_current_song)

        return playlist

    async def currentsong(self) -> dict[str, str]:
        """Get the current status (song being played)"""
        status = {}

        if self.mpd_current_song:
            status['file'] = self.mpd_current_song

        return status

    async def play(self) -> None:
        """Begin / resume / continue playback"""
        self.commands.append('play')

    async def pause(self) -> None:
        """Pause playback"""
        self.commands.append('pause')

    async def next(self) -> None:
        """Play next song"""
        self.commands.append('next')

    async def previous(self) -> None:
        """Play previous song"""
        self.commands.append('previous')

    async def update(self) -> None:
        """Rebuild the database"""
        self.commands.append('update')

    async def volume(self, val: str) -> None:
        """Adjust the volume by VAL percent"""
        self.commands.append(f'volume {val}')

    async def delete(self, pos: int) -> None:
        """Delete item at position POS from the queue"""
        if pos >= len(self.mpd_queue):
            raise CommandError('Bad song index')

        del self.mpd_queue[pos]

    async def albumart(self, path: str) -> dict[str, bytes]:
        """Return album art for the specified PATH"""
        if path not in self.mpd_covers:
            raise CommandError('File not found')

        filename = self.mpd_covers[path]
        with open(f'tests/fixtures/{filename}', 'rb') as file:
            data = file.read()
        return {'binary': data}

    async def idle(self) -> AsyncGenerator:
        """Return a list of recent events"""
        for _ in range(self.idle_queue.qsize()):
            yield await self.idle_queue.get()
