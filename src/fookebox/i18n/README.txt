Update strings:

pot-create -o fookebox/i18n/fookebox.pot fookebox -c lingua.cfg  -F --package-name fookebox --package-version 0.8.0


Update translation template:

msgmerge --update -q de/LC_MESSAGES/fookebox.po fookebox.pot


Update binary files:

msgfmt -o de/LC_MESSAGES/fookebox.mo de/LC_MESSAGES/fookebox.po
