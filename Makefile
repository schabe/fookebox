.PHONY: check
check:
	pytest --cov=fookebox

.PHONY: install
install:
	pip install .

.PHONY: clean
clean:
	rm -rf build/

.PHYON: dev
dev:
	pip install -e ".[dev]"
	pip install -e ".[test]"

.PHONY: dist
dist:
	python3 -m build -s

.PHONY: lint
lint:
	pylint --py-version=3.9 src/fookebox/ tests/

	flake8 src/fookebox tests/

	mypy --ignore-missing-imports --disallow-untyped-defs src/fookebox tests

	eslint src/fookebox/js/i18n-de.js \
		src/fookebox/js/i18n.js \
		src/fookebox/js/fookebox.js
